CFLAGS ?= -O0 -g
all: patch-index.tgz.pdf \
     patch-index.tgz \
     patch-index.tgz.c \
     patch-index.tgz.tex
%.tex: %.w
	cweave $<
%.c: %.w
	ctangle $<
%.pdf: %.tex
	xetex $<
.PHONY: all
