\def\+#1{$\langle${\it #1}$\rangle$}
@* Introduction and problem.

Every Haskell package, uploaded on Hackage, has .cabal file that
contains information, used by {\tt cabal-install\/} program to build
installation plan. Cabal files of all packages, present on Hackage,
are downloaded by {\tt cabal update\/} command and used when deciding,
whether given set of packages is co-installable.

Debian Haskell Group uses {\tt cabal-install\/} to check, whether Debian
packages, derived from given list of Haskell packages, would be
co-installable (as defined by {\tt apt\/} package manager). Since some
of corresponding Debian packages alter cabal-files, there is need
somehow inform {\tt cabal-install} about those alterations.

Program {\tt cabal-install} downloads archive {\tt 01-index.tar}
of cabal files for all packages on Hackage, and there is need
to change that archive to reflect changes made by Debian packages.
This is already being done by {\tt test-packages.pl}, which uses Perl
function |replace_content| from module |Archive::Tar|. Unfortunately,
it works rather slow.

It is important to know, that |tar| archive has no notion of
index~---~there is no better way to find file in archive, but perform
linear seach.  Examination of sources of |Archive::Tar| module reveals,
that it does nothing special to speed up |replace_content| function;
implementation of that function uses linear search, and, as such has
$O(n)$ complexity, where $n$ is number of files in archive. At time of
writing (2018-09-03) there are 209250 files in {\tt 01-index.tar},
and it is growing. Further more, there is 287 patches at time of writing,
that means that current implementation performs no less than 287 scans of
huge (508 Mb) tar archive. There must be better way.

Instead of iterating all patches, and performing update of tar archive
for every patch applied, program should iterate entries in tar archive,
and apply patch, if it exist for given entry. This way whole archive is
travesed only once, effectively changing complexity of algrorithm from
$O(N * M)$ to $O(N + M)$, where $N$ is number of entries in archive, and
$M$ is number of patches.

Another possible (mirco-)optimization would be editing tar archive
in-place. Files in tar archives are stored in blocks of 512 files, so,
if patching file does not change number of blocks occupied, it
is possible to change archive in-place. Unfortunately, in
particular case of archive with cabal files, this optimization is not
applicable -- there is files of length exactly 512 bytes, and patching
them makes them bigger, changing number of blocks occupied.

@* Interface of new implementation.

This program, named \.{patch-tar}, solves a different formulation
of the problem, described above. Instead of tying itself to directory
layout, used by Debian Haskell Team, this program tries to be of more
general use. It has single argument \+{patchdir}; every regular file
in archive with \+{filepath} is patched with
\.{\+{patchdir}/\+{filepath}.patch}, if the latter exists.
Input archive is read from stdin, output archive is written to stdout.
Neither stdin nor stdout are expected to be seekable.

@* Implementation.

Program follows usual layout for C language:

@c

@<Headers@>@/
@<Types@>@/
@<Utilities@>@/
@<Function to process single file in archive@>@/
@<main@>@/

@ Here is outline of utility function
@<Utilities@>=
@<fatal() function@>@/
@<construct-patch-name() function@>@/
@<invoke-patch() function@>@/
@<octal-parsing functions@>
@<regenerate-checksum() function@>@/

@ Tar archive is composed of blocks of 512 bytes, where some blocks are
headers, followed by zero or more blocks of raw data. Below is C
structure, created from description in \.{tar.h} in GNU Tar.

@<Types@>=
struct tar_header {
	char name[100];     // string
	char mode[8];       // number
	char uid[8];        // number
	char gid[8];        // number
	char size[12];      // number
	char mtime[12];     // number
	char chksum[8];     // number
	char typeflag;      // enumeration
	char linkname[100]; // string
	char magic[6];      // magic string
	char version[2];    // magic string
	char uname[32];     // string
	char gname[32];     // string
	char devmajor[8];   // number
	char devminor[8];   // number
	char prefix[155];   // optional string
	char ___pading[12];  // unused
};

@ Strangely, but tar archive header does not use all 512 bytes~---~size
of all fields together yields only 500 bytes, while |chksum| field is
derived from all 512 bytes. For convenience, we add 12 bytes of padding
and make sure, that now |tar_header| structure have correct size.
@<Types@>=
extern char ___tar_header_check[sizeof(struct tar_header) == 512 ? 1 : -1];

@ All operations with tar files are chunked by block of 512 bytes. Let
us define structure, that represent that block.

@<Types@>=
struct block { char bytes[512]; };

@ To avoid global variables, here we define structure, that contains
  everything needed.
@<Types@>=
struct continuation {
	struct block current;
	char   patch[100 + sizeof(PATCH_SUFFIX) + 1];
	int    infd;
	int    outfd;
};

@ To store patched version of file in archive, there will be need to
write its new size as octal number; when skipping over data blocks,
there is need to know file size as integer. So there is need of
functions, converting from octal string from/to integer.
There is huge range of interger types in C, here I will use |off_t|,
which is type used by |stat| function to report file size, hence the
includes.

@s off_t int
@<Headers@>=
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

@ 
@<octal-parsing functions@>=
off_t octal_to_integral(char octal[8]) {
	off_t res = 0;
	for (char *p = octal; *p; ++p) {
		unsigned short int digit = *p - '0';
		res = res * 8 + digit;
	}
	return res;
}

void integral_to_octal(char octal[8], off_t N)
{
	for (int i = 6; i >= 0; i--) {
		octal[i] = '0' + (N % 8);
		N /= 8;
	}
	octal[7] = '\0';
}

@ Patched file have different size, which invalidates checksum of tar
header. This function adjust checksum to correct value.

@<regenerate-checksum() function@>=
void regenerate_checksum(struct tar_header *header)
{
	unsigned char *p = (void*)header;
	int chksum = 0;
	memset(header->chksum, ' ', 8);
	for (int i = 0; i != 512; ++i)
		chksum += p[i];
	integral_to_octal(header->chksum, chksum);
}

@ Next, functions, operating on block of size 512 bytes.

@<Headers@>=
#include <unistd.h>

@ 
@<Utilities@>=
void *read_block(struct block *b, int fd)
{
	ssize_t bytes = read(fd, b,  sizeof(*b));
	return (bytes == sizeof(*b)) ? b : NULL;
}

void write_block(const struct block *b, int fd)
{
	write(fd, b, sizeof(*b));
}

@ Tar archive ends with two blocks of zero. Function |block_null| checks
  whether |the_block| is zero.
@<Headers@>=
#include <string.h>
@ 
@<Utilities@>=
int block_null (const struct block *b)
{
	static char null_block[sizeof(*b)] = {0};
	return !memcmp(b, null_block, sizeof(*b));
}

@ This is function to report errors. I do not foresee different kinds
  of errors, all errors are considered fatal.
@d ERR_PREFIX "patch-index: "
@d write2(x) write(2, x, sizeof(x) - 1)
@<fatal() function@>=
void fatal(const char *message)
{
	write2(ERR_PREFIX);
	write(2, message, strlen(message));
	exit(1);
}

@ Add header to use |exit| function.
@<Headers@>=
#include <stdlib.h>

@ Function |construct_patch_name|, that appends {\tt .patch}
  suffix to name. Since name of archive member is at most 100 bytes
  (not counting stupid |prefix| hack), 107 bytes will suffice for patch
  name.

@d PATCH_SUFFIX ".patch"
@<construct-patch-name() function@>=
const char* construct_patch_name(char *buffer, const char *name)
{
	size_t len = strnlen(name, 100);
	memmove(buffer, name, len);
	memmove(buffer + len, PATCH_SUFFIX, sizeof(PATCH_SUFFIX));
	return buffer;
}

@ Now, with preparations are done, function that actually process files
in archive. Function |process_file| returns 1 if it successfully
processed file (written corresponding entry on stdout, patched or not),
and returns 0, if there is no more files.

Here we treat failure to read a block or null block equally as sign of
archive end. This is more liberal, than prescribed by standard, but
allows us to read tar achives without two final null blocks. Such
archives, has nice property~---~join of their content is just plain file
concatenation.

@d MSG_FAIL_HEADER "failed to head header\n"
@<Function to process single file in archive@>=

int process_file(struct continuation *cont) {
	struct tar_header *header = read_block(&cont->current, cont->infd);

	if (!header || block_null(&cont->current)) {
		return 0;
	}

	if (!regular_file(header)) {
		write_block(&cont->current, cont->outfd);
		return 1;
	}

	off_t fsize = octal_to_integral(header->size);

	construct_patch_name(cont->patch, header->name);

	(access(cont->patch, R_OK) == 0)
		? patch_entry(cont, fsize)
		: copy_entry(cont, fsize);
	return 1;
}

@ 
@d MSG_FAIL_BLOCK "failed to read block"

@<Utilities@>=
void copy_blocks(struct continuation *cont, int n)
{
	for (int i = 0; i != n; ++i) {
		if (!read_block(&cont->current, cont->infd))
			fatal(MSG_FAIL_BLOCK);
		write_block(&cont->current, cont->outfd);
	}
}
@ Non-regular files are represented solely by header, and can not be
patched. As such, they are copied as-is.

@d REGTYPE '0'
@d AREGTYPE '\0'

@<Utilities@>=
int regular_file(const struct tar_header *h)
{
	char type = h->typeflag;
	return (type == REGTYPE) || (type == AREGTYPE);
}

@ If there is no patch file for given entry, header and all
corresponding blocks are just copied from |infd| to |outfd|.
@d NBLOCKS(size) (size / 512) + (size % 512 != 0)
@<Utilities@>=
void copy_entry(struct continuation *cont, off_t fsize) {

	int nblocks = NBLOCKS(fsize);
	write_block(&cont->current, cont->outfd);
	copy_blocks(cont, nblocks);
}

@ Before invoking \.{patch} utility, we must write file to be patched
into file system.

@<Utilities@>=
void write_original_file(struct continuation *cont, off_t fsize)
{
	struct tar_header *header = (void *)&cont->current;
	int fd = open(header->name, O_WRONLY|O_CREAT, 0644);
	if (fd == -1)
		return fatal(header->name);

	for (int i = 0; i != fsize/512; ++i) {
		struct block b;
		read_block(&b, cont->infd);
		write_block(&b, fd);
	}
	off_t tail = fsize % 512;
	if (tail) {
		struct block b;
		read_block(&b, cont->infd);
		write(fd, b.bytes, tail);
	}
	close(fd);
}

@ Function |xwaitpid| waits for given pid, and invokes |fatal| if
something go wrong.

@<Utilities@>=
void xwaitpid(pid_t pid)
{
	int status;
	if (waitpid(pid, &status, 0) == -1)
		fatal(MSG_FAIL_WAITPID);
	if (! (WIFEXITED(status) && WEXITSTATUS(status) == 0))
		fatal(MSG_FAIL_PATCH);
}

@
@<Utilities@>=
int open_read(const char *name, off_t *size)
{
	int fd = open(name, O_RDONLY);
	struct stat sb;

	if (fd == -1) fatal(MSG_FAIL_OPEN_READ);
	if (fstat(fd, &sb) == -1) fatal(MSG_FAIL_STAT);
	*size = sb.st_size;
	return fd;
}


@ When function |patch_entry| is called, |cont->current| contains header,
corresponding to regular file of size |fsize|, that have patch.

@d MSG_FAIL_FORK "Failed to fork()\n"
@d MSG_FAIL_WAITPID "Failed to waitpid()\n"
@d MSG_FAIL_PATCH "Failed to patch\n"
@d MSG_FAIL_OPEN_READ "Failed to open for reading\n"
@d MSG_FAIL_STAT "Failed to stat()\n"

@<Utilities@>=
void patch_entry(struct continuation *cont, off_t fsize)
{
	struct tar_header *header = (void *)&cont->current;

	write_original_file(cont, fsize);

	pid_t pid = fork();
	if (pid == -1)
		fatal(MSG_FAIL_FORK);
	pid ? xwaitpid(pid) : invoke_patch(header->name);


	off_t newsize;
	int fd = open_read(header->name, &newsize);

	integral_to_octal(header->size, newsize);
	regenerate_checksum(header);
	write_block(&cont->current, cont->outfd); // header

	/* Read new content from |fd| */
	for (int i = 0; i != NBLOCKS(newsize); ++i) {
		struct block b;
		read_block(&b, fd);
		write_block(&b, cont->outfd);
	}
	close(fd);
	unlink(header->name);
}

@ Operations with file descriptors (|open|, |close|) require additional
headers
@<Headers@>=
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>

@ According to POSIX, functions |dirname| and |basename| are defined
withing following header
@<Headers@>=
#include <libgen.h>

@ Execute \.{patch} utility. This function is called in subprocess.
@d MSG_FAIL_CHDIR "Failed to change directory\n"
@<invoke-patch() function@>=
void invoke_patch(const char *filename)
{
	char buffer[100];
	char patch[120];
	char *file;
	strncpy(buffer, filename, 100);
	if (chdir(dirname(buffer)) == -1)
		fatal(MSG_FAIL_CHDIR);

	strncpy(buffer, filename, 100);
	file = basename(buffer);
	construct_patch_name(patch, file);
	close(0);
	close(1);
	close(2);
	if (execlp("patch", "patch", "-sp1", file, patch, NULL) == -1)
		fatal(MSG_FAIL_PATCH);
}

@ This is main function, to stop make(1) compaining.

@<main@>=
int main(int argc, char **argv)
{
	struct continuation cont;
	cont.infd = 0;
	cont.outfd = 1;
	while (process_file(&cont))
		;
	return 0;
}
